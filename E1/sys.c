/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>

#include <utils.h>

#include <io.h>

#include <mm.h>

#include <mm_address.h>

#include <sched.h>

#include <errno.h>

#include <system.h>

#include <entry.h>

#define LECTURA 0
#define ESCRIPTURA 1

char destino[256];

int check_fd(int fd, int permissions)
{
  if (fd!=1) return -9; /*EBADF*/
  if (permissions!=ESCRIPTURA) return -13; /*EACCES*/
  return 0;
}

int sys_ni_syscall()
{
	return -38; /*ENOSYS*/
}

int sys_getpid()
{
	return current()->PID;
}

int ret_from_fork()
{
	return 0;
}

int sys_fork()
{
  int PID=-1;
  struct list_head *hijo_lh;
  struct task_struct *hijo_ts;
  // creates the child process
  if(list_empty(&freequeue))
	  return -ENOMEM;
  hijo_lh = list_first(&freequeue);
  hijo_ts = list_head_to_task_struct(hijo_lh);
  list_del(hijo_lh);
  //copiamos PCB de padre a hijo
  copy_data(current(), hijo_ts, sizeof(union task_union));
  allocate_DIR(hijo_ts);
  page_table_entry * hijo_pt = get_PT(hijo_ts);
  page_table_entry * padre_pt = get_PT(current());

  int i, frame;
  int frames_data[NUM_PAG_DATA];
  for(i = 0; i < NUM_PAG_DATA; i++) //reservar espacio para data y stack
  {
	  frame = alloc_frame();
	  if(frame < 0){
		  int j;
		  for (j = 0; j < i; j++){ //la marcha atras
			  free_frame(frames_data[j]);
		  	  del_ss_pag(hijo_pt, PAG_LOG_INIT_DATA+j);
		  }
	          list_add_tail(hijo_lh, &freequeue);
		  return -ENOMEM;
	  }
	  frames_data[i] = frame;
	  set_ss_pag(hijo_pt, PAG_LOG_INIT_DATA+i, frame);
  }

  for(i = 0; i < NUM_PAG_DATA; i++)
  {
	set_ss_pag(padre_pt, PAG_LOG_INIT_DATA+NUM_PAG_DATA+i, frames_data[i]); //asocias pag del padre al final a frame del hijo
	//copias los datos del padre a los frames del hijo que has asociado aqui al padre
	copy_data((unsigned int *)((PAG_LOG_INIT_DATA+i)<<12),(unsigned int *)((PAG_LOG_INIT_DATA+NUM_PAG_DATA+i)<<12),PAGE_SIZE); 
	del_ss_pag(padre_pt, PAG_LOG_INIT_DATA+NUM_PAG_DATA+i); //liberas el espacio logico del padre que habias reservado pal hijo
  }


  //las 256+8 primeras paginas son compartidas(codigo kernel + codigo user)
  //luego las 20 de stack + data son privadas por proceso
 
  for(i = 1; i < NUM_PAG_KERNEL+1; i++)
  {
	hijo_pt[i].entry = padre_pt[i].entry;
  }
  for(i = PAG_LOG_INIT_CODE; i < NUM_PAG_CODE; i++)
  {
	hijo_pt[i].entry = padre_pt[i].entry;  
  }
	
  set_cr3(get_DIR(current()));
  PID = ++GLOBAL_PID;
  hijo_ts->PID = PID;
  hijo_ts->state = ST_READY;
  set_quantum(hijo_ts, 10);
  union task_union *hijo_tu;
  hijo_tu = (union task_union *)hijo_ts;

  int dif = (get_ebp() - (int) current())/sizeof(int); 
  hijo_tu->stack[dif] = (unsigned int)&ret_from_fork;
  hijo_tu->stack[dif-1] = 0;
  hijo_ts->kernel_esp = (unsigned int)&hijo_tu->stack[dif-1]; 

  list_add_tail(hijo_lh, &readyqueue); 
  return PID;
}

void sys_exit()
{
	free_user_pages(current());
	current()->PID = -1;
	update_process_state_rr(current(), &freequeue);
	sched_next_rr();
}

int sys_write(int fd, char *buffer, int size)
{
	int e = check_fd(fd, ESCRIPTURA);
	if (e != 0)
		return e;
	if(buffer == NULL)
		return -EINVAL;
	if(size < 0)
		return -EINVAL;

	int counter = 0;
	while(counter < size){
		if (size - counter >= 256){
			if(copy_from_user(buffer, destino, 256) != 0)
				return -EIO;
			counter += 256;
			buffer += 256;
			sys_write_console(destino, 256);
		}
		else {
			if(copy_from_user(buffer, destino, size-counter) != 0)
				return -EIO;
			sys_write_console(destino, size-counter);
			counter =  size;
		}
	}
	return size;
}

int sys_gettime()
{
	return zeos_ticks;
}
