/*
 * entry.h - Definició del punt d'entrada de les crides al sistema
 */

#ifndef __ENTRY_H__
#define __ENTRY_H__

void keyboard_handler();
void clock_handler();
void syscall_handler_sysenter();
void writeMSR(int number, int value);
void task_switch(union task_union *new);
void set_esp(unsigned int esp);
unsigned int get_ebp();
#endif  /* __ENTRY_H__ */
