/*
 * sched.c - initializes struct for task 0 anda task 1
 */

#include <sched.h>
#include <mm.h>
#include <io.h>
#include <entry.h>

union task_union task[NR_TASKS]
  __attribute__((__section__(".data.task")));

struct task_struct *idle_task;
struct task_struct *init_task;

int quantum_global;

struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  //return list_entry( l, struct task_struct, list);
  return (struct task_struct *)((unsigned int)l&0xfffff000);
}


extern struct list_head blocked;


/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry * get_DIR (struct task_struct *t) 
{
	return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry * get_PT (struct task_struct *t) 
{
	return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}


int allocate_DIR(struct task_struct *t) 
{
	int pos;

	pos = ((int)t-(int)task)/sizeof(union task_union);

	t->dir_pages_baseAddr = (page_table_entry*) &dir_pages[pos]; 

	return 1;
}

void cpu_idle(void)
{
	__asm__ __volatile__("sti": : :"memory");

	while(1){}
}

void init_idle (void)
{
	union task_union *idle_tu;
	struct task_struct *idle_ts;
	struct list_head *idle_lh;
	idle_lh = list_first(&freequeue);
	idle_ts = list_head_to_task_struct(idle_lh);
	list_del(idle_lh);
	idle_ts->PID = 0;
	idle_ts->state = ST_READY;
	set_quantum(idle_ts, 5);
	allocate_DIR(idle_ts);
	//execution context inicialization
	idle_tu = (union task_union *)idle_ts;
	idle_tu->stack[KERNEL_STACK_SIZE-1] = (unsigned int)cpu_idle;
	idle_tu->stack[KERNEL_STACK_SIZE-2] = 0;
	idle_tu->task.kernel_esp = (unsigned int)&idle_tu->stack[KERNEL_STACK_SIZE-2];
	idle_task = idle_ts;
		
}

void init_task1(void)
{
	struct task_struct *init_ts;
	struct list_head *init_lh;
	init_lh = list_first(&freequeue);
	init_ts = list_head_to_task_struct(init_lh);
	list_del(init_lh);
	init_ts->PID = 1;
	init_ts->kernel_esp = INITIAL_ESP;
	init_ts->state = ST_RUN;
	set_quantum(init_ts, 10);
	allocate_DIR(init_ts);
	set_user_pages(init_ts);
	tss.esp0 = INITIAL_ESP;
	writeMSR(0x175, INITIAL_ESP);
	set_cr3(init_ts->dir_pages_baseAddr);
	init_task = init_ts;
}


void init_freequeue()
{
	INIT_LIST_HEAD(&freequeue);
	int i;
	for(i=0; i < NR_TASKS; i++)
	{
		list_add_tail(&task[i].task.list, &freequeue);
	}
}

void init_readyqueue()
{
	INIT_LIST_HEAD(&readyqueue);
}

void init_sched()
{
	init_freequeue();
	init_readyqueue();
}

struct task_struct* current()
{
  int ret_value;
  
  __asm__ __volatile__(
  	"movl %%esp, %0"
	: "=g" (ret_value)
  );
  return (struct task_struct*)(ret_value&0xfffff000);
}

void inner_task_switch(union task_union *new)
{
	set_cr3(new->task.dir_pages_baseAddr);
	tss.esp0 = KERNEL_ESP(new);
	writeMSR(0x175, KERNEL_ESP(new));
	current()->kernel_esp = (unsigned int)get_ebp();
	set_esp(new->task.kernel_esp);
	return;
}

void update_sched_data_rr ()
{
	--quantum_global;
}

int needs_sched_rr ()
{
	if(quantum_global == 0) return 1;
	else return 0;
}

void update_process_state_rr(struct task_struct *t, struct list_head *dst_queue)
{
        enum state_t estado = t->state;
        if(dst_queue == NULL){
                if(estado == ST_READY){
                 list_del(&t->list);
                 t->state = ST_RUN;
                }
        }
        else{
                if(estado == ST_RUN){
                        if(dst_queue == &readyqueue) t->state = ST_READY;
                        if(dst_queue == &freequeue) t->state  = ST_FREE;
                        list_add_tail(&(t->list), dst_queue);
                }
        }
}

void sched_next_rr()
{
        if(list_empty(&readyqueue)){
		quantum_global = get_quantum(idle_task);
		if(current() != idle_task) task_switch((union task_union *) idle_task);
        }
        else{
                struct task_struct* new_ts = list_head_to_task_struct(list_first(&readyqueue));
		quantum_global = get_quantum(new_ts);
		update_process_state_rr(new_ts, NULL);
                task_switch((union task_union *) new_ts);
        }
}

void sched()
{
	update_sched_data_rr();
	if(needs_sched_rr()){
	       	if(current() != idle_task) update_process_state_rr(current(), &readyqueue);
		sched_next_rr();
	}
}

void set_quantum (struct task_struct *t, int new_quantum)
{
	t->quantum = new_quantum;
}

int get_quantum(struct task_struct *t)
{
	return t->quantum;
}
